import './App.css';
import {BrowserRouter as Router, Routes, Route} from "react-router-dom";
import Welcome from './pages/Welcome.js';
import Main from './pages/Main.js'


// components

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Welcome/>} />
        <Route exact path="/portfolio" element={<Main />} />
      </Routes>
    </Router>


  );
}

export default App;
