import React from 'react'
import {BrowserRouter as Router, Link} from 'react-router-dom';

import kitty from '../icons/kitty.png'


export default function Welcome(){

	return(
		
		<div className="welcome-container">
				<Link to="/portfolio" className="welcome-wrapper">
				<span className="bubble-wrapper"><i>Click Me!</i></span>
				<img src={kitty} className="welcome-img"/>
			</Link>
		</div>
		
			
		
		)
	
}